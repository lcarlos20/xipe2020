﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Xipe2020.Models;

namespace Xipe2020.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            var apiUrl = "https://thecontent.mx/api";
            string mGuid = "3efce620-50f1-4045-b80a-a0243ef7e60d";
            var uri = apiUrl + "/" + mGuid;
            String newsResponse = String.Empty;

            var studiesCases = 1863;

            try
            {
                using (var webClient = new WebClient())
                {
                    webClient.Headers["SomeHeader"] = "TheContent";
                    webClient.Headers[HttpRequestHeader.UserAgent] = "https://thecontent.mx";
                    webClient.Encoding = Encoding.UTF8;
                    newsResponse = webClient.DownloadString(uri + "/News/GetNewsByMagazine");
                }
            }
            catch (Exception ex)
            {

            }

            var newsModel = JsonConvert.DeserializeObject<List<News>>(newsResponse).Where(x => x.CategoryId == studiesCases).ToList();
            var model = new IndexViewModel
            {
                News = newsModel.OrderByDescending(x => x.CreationDate).ToList(),
            };
            return View(model);
        }

        public IActionResult ArticleDetail(string id)
        {
            var apiUrl = "https://thecontent.mx/api";
            string mGuid = "3efce620-50f1-4045-b80a-a0243ef7e60d";
            var uri = apiUrl + "/" + mGuid;
            String newsResponse = String.Empty;

            try
            {
                using (var webClient = new WebClient())
                {
                    webClient.Headers["SomeHeader"] = "TheContent";
                    webClient.Headers[HttpRequestHeader.UserAgent] = "https://thecontent.mx";
                    webClient.Encoding = Encoding.UTF8;
                    newsResponse = webClient.DownloadString(uri + "/News/GetNewsByMagazine");
                }
            }
            catch (Exception ex)
            {

            }

            var article1 = JsonConvert.DeserializeObject<List<ArticleModel>>(newsResponse);
            var article = article1.FirstOrDefault(x => x.Permalink == id);

            var content = new IndexViewModel
            {
                Category = article
            };
            return View(content);
        }

        public IActionResult ContactUs()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
