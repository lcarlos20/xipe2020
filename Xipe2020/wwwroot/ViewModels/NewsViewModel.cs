﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Xipe2020.wwwroot.ViewModels
{
    public class NewsViewModel
    {
        public class NewsViewModel
        {
            [Display(Name = "Título")]
            [Required(ErrorMessage = "{0} es requerido.")]
            public String Title { set; get; }

            [Display(Name = "Descripción")]
            [Required(ErrorMessage = "{0} es requerido.")]
            public String Description { set; get; }

            [AllowHtml]
            [Display(Name = "Contenido")]
            [Required(ErrorMessage = "{0} es requerido.")]
            public String Body { set; get; }

            [Display(Name = "Categoría")]
            [Required(ErrorMessage = "{0} es requerido.")]
            public Int32 CategoryId { set; get; }

            public String LogoImage { set; get; }

            [Display(Name = "Alt. de imágen")]
            [Required(ErrorMessage = "{0} es requerido.")]
            public String Alt { set; get; }

            [Display(Name = "Meta Description")]
            public String MetaDesc { set; get; }

            [Display(Name = "Keywords o Tags")]
            public String MetaTags { set; get; }

            [Display(Name = "Permalink")]
            [Required(ErrorMessage = "{0} es requerido.")]
            public String Permalink { set; get; }

            [AllowHtml]
            [Display(Name = "URL de video")]
            public String VideoEmbed { set; get; }

            public Int32 NewsId { set; get; }
            public Boolean IsClon { get; set; }
            public String ThankNote { get; set; }
        }
    }
}
