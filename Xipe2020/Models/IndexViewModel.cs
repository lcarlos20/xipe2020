﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Xipe2020.Models
{
    public class IndexViewModel
    {
        public List<News> News { get; set; }
        public ArticleModel Category { get; set; }
    }
}
